package com.persona.demo;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import java.util.*;

@Controller
public class PersonaController {
    @RequestMapping(value = "/", method = RequestMethod.GET)
    @ResponseBody
    public String persona(){
        return "Hello World!";
        // List<Integer> lst = new ArrayList<Integer>();
        // lst.add(1);
        // lst.add(2);
        // lst.add(3);
        // for(Integer i: lst){
        //     System.out.println(i);
        // }
        // return lst.toString();
    }
}